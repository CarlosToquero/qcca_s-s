//import ketai.camera.*;
//import ketai.ui.*;

ClassCamera Camara;

int VERSION_DIVISION_RECTANGULO = 2; // 1 - Version inicial, cutre; 2 - Version Tokero 

Button Mas;
Button Menos;

RadioButtons Opciones;

int ANCHO = 640;
int ALTO = 720;

int OpcionSeleccion = 0; //0-recta; 1-circulo, 2-cuadrado

int Trozos = 21;
int MinTrozos = 2;

int Diametro = 400;
int AlturaRectangulo, AnchuraRectangulo;
int AlturaMinima, AnchuraMinima;

boolean logged;

Accelerometer accel;
float proyeccion;

////////////////////////////////////////////////////////////////////////////////
// GENERAL METHODS AND EVENTS
////////////////////////////////////////////////////////////////////////////////
void setup()
{
  //size(ANCHO, ALTO); //en android no hace falta la resolucion (en IOs nose)
  AlturaRectangulo = height / 2;
  AnchuraRectangulo = width / 2;
  AlturaMinima = AlturaRectangulo / 10;
  AnchuraMinima = AnchuraRectangulo / 10;

  // nombre, pos.x, pos.y, width, height
  Menos = new Button("-", 50, 50, 50, 50);
  Mas = new Button("+", width - 100, 50, 50, 50);

  String [] radioNames = {
    "tubo", "pizza", "tarta"
  };
  //nombre de botones, numero de botones, posini.x, posini.y, width, height, orientacion)
  Opciones = new RadioButtons(radioNames, 3, 50, height - 100, 150, 50, HORIZONTAL);

  //textFont(createFont("Georgia", 36));

  orientation(LANDSCAPE);
  //orientation(PORTRAIT); 
  imageMode(CENTER);
  
  println(Camara);
  if (Camara == null){
    Camara = new ClassCamera(this, width, height,  15);
    Camara.start();
  }
  
  logged = false;
  accel = new Accelerometer(this);
}

void draw()
{
  setupDraw();
  showCamera();
  drawButtons();
  drawShape();
  writeText();
}

void mouseReleased()
{
  if (Mas.mouseReleased())
  {
    Trozos += 1;
    logged = false;
  }

  if (Menos.mouseReleased())
  {
    if (Trozos > MinTrozos)
    {
      Trozos -= 1;
      logged = false;
    }
  }

  if (Opciones.mouseReleased())
  {
    if (Opciones.get()>=0) {
      OpcionSeleccion =  Opciones.get();
    }
  }
}

void onCameraPreviewEvent()
{
  Camara.read();
}

////////////////////////////////////////////////////////////////////////////////
// DRAW DIRECTLY RELATED METHODS
////////////////////////////////////////////////////////////////////////////////
void setupDraw() {
  background(60, 60, 100);
  strokeWeight(3);
  fill(255, 255, 255, 0);
  textSize(20);
}

void showCamera() {
  image(Camara, width/2, height/2 );
}

void drawButtons() {
  Mas.display();
  Menos.display();
  Opciones.display();
}

void drawShape() {
  if (OpcionSeleccion == 0)
  {
    Line();
  }
  if (OpcionSeleccion == 1)
  {
    Circle();
  }
  if (OpcionSeleccion == 2)
  {
    Rectangle();
  }
}

void writeText() {
  textSize(50);
  fill(0, 255, 0);
  text(Trozos, (Menos.pos.x + Mas.pos.x )/2, Mas.pos.y + Menos.pos.y );
}

////////////////////////////////////////////////////////////////////////////////
// Draw line related methods
////////////////////////////////////////////////////////////////////////////////
void Line()
{
  line(0, height/2, width, height/2);
  drawLinePieces();
}

void drawLinePieces() {
  int trozo = width/Trozos;
  for (int i = 1; i < Trozos; i++)
  {
    int pendiente = int (((width / 2) - (trozo * i)) / (proyeccion * 40));
    line(trozo*i + pendiente, (height/2)-50, trozo*i - pendiente, (height/2)+50 );
  }
}

////////////////////////////////////////////////////////////////////////////////
// Draw circle related methods
////////////////////////////////////////////////////////////////////////////////
void Circle()
{
  ellipse(width/2, height/2, Diametro, Diametro * proyeccion);
  drawCirclePieces(width/2, height/2, Diametro/2);
}

void drawCirclePieces(int x, int y, int r) {
  float radianesTrozo = TWO_PI/Trozos;
  float pendiente = 0;
  int contador = 1;
  int xfin = x;
  int yfin = y;
  int controlBucle = Trozos;

  if (Trozos%2 == 1) { //Número impar de trozos
    yfin = y - int(r * proyeccion);
    //Dibujamos la línea vertical que une el centro y la parte superior
    line(x, y, xfin, yfin);
  }
  else { //Número par de trozos
    //Dibujamos el diámetro vertical
    line(x, y-(r * proyeccion), x, y+(r * proyeccion));
    controlBucle--;
  }

  //Seguidamente se van pintando el resto de segmentos uno a cada lado:
  while (contador < controlBucle) {
    if (contador%2 == 1) {
      //Se cambia el signo de la pendiente porque el eje y está invertido
      pendiente = -tan(HALF_PI-((contador+1)/2)*radianesTrozo);
      xfin = int(x + r*cos(HALF_PI-((contador+1)/2)*radianesTrozo));
      yfin = int((pendiente*(xfin-x)) * proyeccion + y);
      line(x, y, xfin, yfin);
    }
    else {
      //Se cambia el signo de la pendiente porque el eje y está invertido
      pendiente = -tan(HALF_PI+(contador/2)*radianesTrozo);
      xfin = int(x + r*cos(HALF_PI+((contador+1)/2)*radianesTrozo));
      yfin = int((pendiente*(xfin-x)) * proyeccion + y);
      line(x, y, xfin, yfin);
    }
    contador++;
  }
}

////////////////////////////////////////////////////////////////////////////////
// Draw rectangle related methods
////////////////////////////////////////////////////////////////////////////////
void Rectangle()
{
  drawRectangle();
  if (VERSION_DIVISION_RECTANGULO == 1)
    dibujarTrozos();
  else
    drawRectanglePieces();
}

void drawRectangle(){
  int DLX = width / 2 - AnchuraRectangulo / 2;
  int DY = height / 2 + AlturaRectangulo / 2; 
  int DRX = width / 2 + AnchuraRectangulo / 2;
  int ULX = width / 2 - int(AnchuraRectangulo / 2 * proyeccion);
  int URX = width / 2 + int(AnchuraRectangulo / 2 * proyeccion);
  int UY = height / 2 - AlturaRectangulo / 2;
 // Linea de referencia (inferior)
 line(DLX, DY, DRX, DY);
 // Diagonal izquierda
 line(DLX, DY, ULX, UY);
 // Diagonal derecha
 line(DRX, DY, URX, UY); 
 // Superior
 line(ULX, UY, URX, UY);
}

////////////////////////////////////////////////////////////////////////////////
// Rectangle division algorithm. Version 2.
////////////////////////////////////////////////////////////////////////////////
void drawRectanglePieces() {
  Column[] matrix = generateMatrix();
  drawMatrix(matrix);
}

void drawMatrix(Column[] matrix) {
  if (matrix != null) {
    int vertStartX = width / 2 - int(AnchuraRectangulo / 2 * proyeccion);
    int startX = width / 2 - (AnchuraRectangulo / 2);
    int endX = width / 2 - (AnchuraRectangulo / 2);
    for (int i = 0; i < matrix.length; i++) {
      int Y = height / 4;
      vertStartX += int(matrix[i].cells[0].getWidth() * proyeccion);
      endX += matrix[i].cells[0].getWidth();
      
      // No pintar última línea
      if (i < matrix.length - 1)
        line(vertStartX, Y, endX, Y + AlturaRectangulo);
        
      for (int j = 0; j < matrix[i].cells.length; j++) {
        Y += matrix[i].cells[j].getHeight();
        if (j < matrix[i].cells.length - 1)
          line(startX, Y, endX, Y);
      }
      startX = endX;
    }
  }
  else {
    line(width/4, height/4, 3/4*width, 3/4*height);
  }
}

int calcularAreaDeCadaCelda() {
  int areaTotal = AnchuraRectangulo * AlturaRectangulo;
  int areaTrozo = areaTotal / Trozos;
  return areaTrozo;
}

boolean correctMatrix(Column[] matrix) {
  boolean correcto = true;
  String esCorrecto = "SI";
  for (int i = 0; i < matrix.length; i++) {
    if (!correctColumn(matrix[i])) {
      correcto = false;
      esCorrecto = "NO";
    }
    //println("Columna correcta? " + esCorrecto);
  }
  return correcto;
}

boolean correctColumn(Column c) {
  boolean correcto = true;
  for (int i = 0; i < c.cells.length; i++) {
    //println("Celda " + i + ": ALTURA => " + c.cells[i].getHeight() + " vs. "  + AlturaMinima + " ANCHURA => " + c.cells[i].getWidth() + " vs. "  + AnchuraMinima);
    if (c.cells[i].getHeight() < AlturaMinima || 
      c.cells[i].getWidth() < AnchuraMinima)
      correcto = false;
  } 
  return correcto;
}

int [] calcularNumeroFilasPorColumna(int numeroColumnas) {
  int[] columnas = new int[numeroColumnas];
  int numeroEstandarFilas = Trozos / numeroColumnas;
  int contadorCeldas = 0;
  int i;
  for (i = 0; i < numeroColumnas - 1; i++) {
    columnas[i] = numeroEstandarFilas;
    contadorCeldas += columnas[i];
  }
  columnas[i] = Trozos - contadorCeldas;
  return columnas;
}

Column generarColumnaDeDistribucion(int numFilas, int altura, int anchura) {
  Column distribucion = new Column();
  for (int j = 0; j < numFilas; j++) {
    Cell c = new Cell(altura, anchura);
    distribucion.Add(c);
  }
  return distribucion;
}

Column[] calcularDistribucion(int[] numeroFilasPorColumna) {
  int alturaCeldasUltimaColumna, anchuraCeldasUltimaColumna, alturaCeldasRestoColumnas, anchuraCeldasRestoColumnas;
  Column[] distribucion;

  distribucion = new Column[numeroFilasPorColumna.length];
  alturaCeldasUltimaColumna = AlturaRectangulo / numeroFilasPorColumna[numeroFilasPorColumna.length - 1];
  anchuraCeldasUltimaColumna = calcularAreaDeCadaCelda() / alturaCeldasUltimaColumna;

  if (numeroFilasPorColumna.length == 1) {
    distribucion[0] = generarColumnaDeDistribucion(numeroFilasPorColumna[0], alturaCeldasUltimaColumna, anchuraCeldasUltimaColumna);
  }
  else {
    alturaCeldasRestoColumnas = AlturaRectangulo / numeroFilasPorColumna[0];
    anchuraCeldasRestoColumnas = (AnchuraRectangulo - anchuraCeldasUltimaColumna) / (numeroFilasPorColumna.length - 1);
    int i;
    for (i = 0; i < numeroFilasPorColumna.length - 1; i++) {
      distribucion[i] = generarColumnaDeDistribucion(numeroFilasPorColumna[i], alturaCeldasRestoColumnas, anchuraCeldasRestoColumnas);
    }
    distribucion[i] = generarColumnaDeDistribucion(numeroFilasPorColumna[i], alturaCeldasUltimaColumna, anchuraCeldasUltimaColumna);
  }
  return distribucion;
}

Column[] generateMatrix() {
  int numeroColumnas = 1;
  int[] numeroFilasPorColumna;
  Column[] columnas = null;
  boolean correcto = false;

  while (!correcto && numeroColumnas < 20) {
    numeroFilasPorColumna = calcularNumeroFilasPorColumna(numeroColumnas);
    columnas = calcularDistribucion(numeroFilasPorColumna);
    if (correctMatrix(columnas)){
      correcto = true;
      logMatrix(columnas);
    }
    else
      numeroColumnas++;
  }
  return columnas;
}

void logMatrix(Column[] matrix) {
  if (!logged) {
    println("INICIO DE DISTRIBUCION [" + matrix.length + " columnas]");
    for (int i = 0; i < matrix.length; i++) {
      println("Columna " + i + " (" + matrix[i].cells.length + " filas)");
      for (int j = 0; j < matrix[i].cells.length; j++) {
        print("\t[" + matrix[i].cells[j].getHeight() + ", " + matrix[i].cells[j].getWidth() + "]");
      }
      println("");
    } 
    println("FINAL DE DISTRIBUCION");
  }
  logged = true;
}

////////////////////////////////////////////////////////////////////////////////
// Rectangle division algorithm. Version 1.
////////////////////////////////////////////////////////////////////////////////
int calcularNumeroTrozosVerticales() {
  int numeroVerticales = 1;
  while ( (height / 2) / (Trozos / numeroVerticales) < AlturaMinima) {
    numeroVerticales++;
  }
  return numeroVerticales;
}

int [] calcularAnchuraVerticales() {
  int verticales = calcularNumeroTrozosVerticales();
  int[] anchuras; 
  if (Trozos%verticales == 0) {
    anchuras = new int[verticales]; 
    for (int i = 0; i < verticales; i++) {
      anchuras[i] = (width / 2) / verticales;
    }
  }
  else {
    anchuras = new int[1];
    anchuras[0] = width / 2;
  }
  return anchuras;
}

void dibujarTrozosVerticales(int[] anchuras) {
  if (anchuras.length > 0) {
    int origen = width / 4;
    for (int i = 1; i < anchuras.length; i++) {
      line(origen + anchuras[i], height/4, origen + anchuras[i], 3*height/4);
      origen += anchuras[i];
    }
  }
}

void dibujarTrozosHorizontales(int verticales, int origen, int anchura) {
  int numTrozos = Trozos / verticales; 
  int altura = (height / 2 ) / numTrozos;
  for (int i = 1; i < numTrozos; i++) {
    line(origen, (height/4) + (i*altura), origen + anchura, (height/4) + (i*altura));
  }
}

void dibujarTrozos() {
  int[] anchuraVerticales = calcularAnchuraVerticales();
  dibujarTrozosVerticales(anchuraVerticales);
  int origen = width / 4;
  for (int i = 0; i < anchuraVerticales.length; i++) {
    dibujarTrozosHorizontales(anchuraVerticales.length, origen, anchuraVerticales[i]);
    origen += anchuraVerticales[i];
  }
}

public void accelerationEvent(float x, float y, float z) {
  if (width > height)
    proyeccion = map(abs(x), 0, 10, 1, 0.25);
  else
    proyeccion = map(abs(y), 0, 10, 1, 0.25);
  redraw();
}

