class Cell {
  int _height;
  int _width; 

  Cell(int h, int w) {
    this._height = h;
    this._width = w;
  }
  void setAltura(int h) {
    _height = h;
  }
  void setAnchura(int w) {
    _width = w;
  }
  int getHeight() {
    return _height;
  }
  int getWidth() {
    return _width;
  }
}

class Column {
  Cell[] cells; 

  Column() {
    cells = new Cell[0];
  }

  Column(Cell c) {
    cells = new Cell[1];
    cells[0] = c;
  }

  Column(Cell[] cs) {
    cells = cs;
  }

  void Add(Cell c) {
    Cell[] newCells = new Cell[cells.length + 1];
    int i;
    for (i = 0; i < cells.length; i++) {
      newCells[i] = cells[i];
    }
    newCells[i] = c;
    cells = newCells;
  }
}

